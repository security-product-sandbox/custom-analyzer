# custom-analyzer

Testing GitLab Security Dashboard (SAST, DAST, Container Scanning, Dependency Scanning etc.) with custom (third-party) analyzers

# License

See [LICENSE](./LICENSE).
